import * as cv from 'opencv4nodejs';
import { VideoProcessingMiddleware, VideoProcessingCtx } from '../types';
import { exit } from '../utils/exit';
import { rewind } from '../utils/rewind';

export class KeyboardHandler implements VideoProcessingMiddleware {
  public do(ctx: VideoProcessingCtx) {
    const key = cv.waitKey(1);
    switch (key) {
      case 27: // ESC
        exit(ctx);
        break;
      case 2: // left arrow
        rewind(ctx, -5000);
        break;
      case 3: // right arrow
        rewind(ctx, 5000);
        break;
      case -1:
        // no key pressed
        break;
    }
  }
}


import * as cv from 'opencv4nodejs';
import { VideoProcessingMiddleware, VideoProcessingCtx } from '../types';
import { drawRedRect, drawGreenRect } from '../utils/drawRect';

export class CcDetectFaces implements VideoProcessingMiddleware {
  private ffClassifier: cv.CascadeClassifier;
  private pfClassifier: cv.CascadeClassifier;

  constructor() {
    // Classifiers
    // this.classifier = new cv.CascadeClassifier(cv.HAAR_FRONTALFACE_DEFAULT);
    // this.classifier = new cv.CascadeClassifier(cv.HAAR_FRONTALFACE_ALT);
    this.ffClassifier = new cv.CascadeClassifier(cv.HAAR_FRONTALFACE_ALT2);
    this.pfClassifier = new cv.CascadeClassifier(cv.HAAR_PROFILEFACE);
  }

  do(ctx: VideoProcessingCtx) {
    if (ctx.motion) {
      // restrict minSize and scaleFactor for faster processing
      const options = {
        // minSize: new cv.Size(40, 40),
        // scaleFactor: 1.2,
        scaleFactor: 1.1, // better then 1.2
        minNeighbors: 10
      };

      this.detectFrontalFace(ctx, options);
      this.detectProfileFace(ctx, options);
    }
  }

  detectFrontalFace(ctx: VideoProcessingCtx, options: any) {
    const objects = this.ffClassifier.detectMultiScale(ctx.currentFrame.bgrToGray(), <any>options).objects;

    if (objects.length) {
      objects.forEach(faceRect => drawGreenRect(ctx.currentFrame, faceRect));
    }
  }

  detectProfileFace(ctx: VideoProcessingCtx, options: any) {
    const objects = this.pfClassifier.detectMultiScale(ctx.currentFrame.bgrToGray(), <any>options).objects;

    if (objects.length) {
      objects.forEach(faceRect => drawRedRect(ctx.currentFrame, faceRect));
    }
  }
}

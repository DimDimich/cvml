import * as cv from 'opencv4nodejs';
import { VideoProcessingMiddleware, VideoProcessingCtx } from '../types';
import { drawRedRect, drawGreenRect } from '../utils/drawRect';

import * as tf from '@tensorflow/tfjs-node';
import { Rect } from 'opencv4nodejs';

export class DnnDetectFaces implements VideoProcessingMiddleware {
  public size = new cv.Size(300, 300);
  public vec3 = new cv.Vec3(0, 0, 0);

  constructor(public net: cv.Net) {}

  do(ctx: VideoProcessingCtx) {
    // if (ctx.motion) {
      console.time('frame')

      const frame = ctx.currentFrame;

      const blob = cv.blobFromImage(frame, 1.0, this.size);

      this.net.setInput(blob);

      const out = this.net.forward();
      console.clear();
      console.log(out)

      console.timeEnd('frame');

      ctx.currentFrame = frame;
    // }
  }

}



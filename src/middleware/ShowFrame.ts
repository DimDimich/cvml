import * as cv from 'opencv4nodejs';
import { VideoProcessingMiddleware, VideoProcessingCtx } from '../types';

export class ShowFrame implements VideoProcessingMiddleware {
  public do(ctx: VideoProcessingCtx) {
    const text: cv.TextLine[] = [{
      text: `${ctx.video.get(cv.CAP_PROP_POS_MSEC).toFixed(0)}`,
      fontSize: 2,
      thickness: 2,
      fontType: cv.FONT_HERSHEY_PLAIN,
      lineType: cv.LINE_4,
      color: new cv.Vec3(0, 0, 255)
    }];

    const frame = cv.drawTextBox(ctx.currentFrame, {x: 10, y: 10}, text, 1);
    cv.imshow('frame', frame);
  }
}

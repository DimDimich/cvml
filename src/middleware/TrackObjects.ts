
import * as cv from 'opencv4nodejs';
import { VideoProcessingMiddleware, VideoProcessingCtx } from '../types';

export class TrackObjects implements VideoProcessingMiddleware {
  private _structuring = cv.getStructuringElement(cv.MORPH_ELLIPSE, new cv.Size(3, 3));
  private _bgSubtractor = new cv.BackgroundSubtractorMOG2();


  // do(ctx: VideoProcessingCtx) {
  //   const contours = ctx.currentFrame
  //     .resizeToMax(800)
  //     .absdiff(ctx.firstFrame.resizeToMax(800))
  //     .cvtColor(cv.COLOR_BGR2GRAY)
  //     .gaussianBlur(new cv.Size(5, 5), 10, 10)
  //     .threshold(30, 255, cv.THRESH_BINARY)
  //     .dilate(this._structuring, new cv.Point2(-1, -1), 1)
  //     .resizeToMax(ctx.currentFrame.cols)
  //     .findContours(cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE);

  //   for(let c of contours) {
  //     const {x, y, width, height} = c.boundingRect();
  //     if (c.area < 600 || c.area > 30000) {
  //       continue;
  //     }
  //     ctx.currentFrame.drawRectangle(new cv.Point2(x, y), new cv.Point2(x + width, y + height), new cv.Vec3(0, 0, 255));
  //   }
  // }

  do(ctx: VideoProcessingCtx) {
    const frame = ctx.currentFrame.resizeToMax(1200);
    const foreGroundMask = this._bgSubtractor.apply(frame);
    const dilated = foreGroundMask.dilate(
      cv.getStructuringElement(cv.MORPH_ELLIPSE, new cv.Size(3, 3)),
      new cv.Point2(-1, -1),
      2
    );
    const blurred = dilated.blur(new cv.Size(20, 20));
    const thresholded = blurred.threshold(200, 255, cv.THRESH_BINARY);

    const minPxSize = 4000;
    ctx.motion = drawRectAroundBlobs(thresholded, frame, minPxSize);

    // cv.imshow('foreGroundMask', foreGroundMask);
    // cv.imshow('thresholded', thresholded);

    ctx.currentFrame = frame;
  }
}

function drawRectAroundBlobs (binaryImg: cv.Mat, dstImg: cv.Mat, minPxSize: number, fixedRectWidth?: number) {
  const {
    centroids,
    stats
  } = binaryImg.connectedComponentsWithStats();

  let motion = false;

  // pretend label 0 is background
  for (let label = 1; label < centroids.rows; label += 1) {
    const [x1, y1] = [stats.at(label, cv.CC_STAT_LEFT), stats.at(label, cv.CC_STAT_TOP)];
    const [x2, y2] = [
      x1 + (fixedRectWidth || stats.at(label, cv.CC_STAT_WIDTH)),
      y1 + (fixedRectWidth || stats.at(label, cv.CC_STAT_HEIGHT))
    ];
    const size = stats.at(label, cv.CC_STAT_AREA);
    const blue = new cv.Vec3(255, 0, 0);
    if (minPxSize < size) {
      dstImg.drawRectangle(
        new cv.Point2(x1, y1),
        new cv.Point2(x2, y2),
        blue,
        2
      );
      centroids.rows && cv.drawTextBox(dstImg, new cv.Point2(dstImg.sizes[0]-100, 200), [{text: 'Motion'}], 1)
      motion = true;
    }
  }

  return motion;
};

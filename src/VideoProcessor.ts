import * as cv from 'opencv4nodejs';
import { VideoProcessingMiddleware, VideoProcessingCtx } from './types';
import { exit } from './utils/exit';

if (cv.version.minor === 4) {
  console.log('Warning: It seems like opencv 3.4 does not run the opencl version of detectMultiScale.');
}

export class VideoProcessor {
  private middlewares: VideoProcessingMiddleware[] = [];

  public use(m: VideoProcessingMiddleware) {
    this.middlewares.push(m);
  }

  public async run(filePath: string | number) {

    let ctx: VideoProcessingCtx = {
      currentFrame: new cv.Mat(),
      firstFrame: new cv.Mat(),
      video: new cv.VideoCapture(<any>filePath),
      timerID: undefined,
      frameRatio: 0,
    };

    const frame = ctx.video.read();
    const frameRatio = frame.cols / frame.rows

    if (frame.empty) {
      exit(ctx);
      return;
    }

    ctx.currentFrame = frame;
    ctx.firstFrame = frame;
    ctx.frameRatio = frameRatio;

    ctx.timerID = setInterval(() => {
      ctx.currentFrame = ctx.video.read();
      this.middlewares.forEach(m => m.do(ctx));
    }, 1);
  };
}

import * as cv from 'opencv4nodejs';

export interface VideoProcessingCtx {
  readonly video: cv.VideoCapture;
  currentFrame: cv.Mat;
  firstFrame: cv.Mat;
  timerID: NodeJS.Timer | undefined;
  frameRatio: number;
  [key: string]: any;
}

export interface VideoProcessingMiddleware {
  do: (context: VideoProcessingCtx) => void;
}

export interface VideoProcessor {
  middlewares: VideoProcessingMiddleware[];
  run: (file: string) => Promise<void>;
}

import * as cv from 'opencv4nodejs';
import { Vec3, Mat, Rect } from 'opencv4nodejs';

export const drawRect = (image: Mat, rect: Rect, color: Vec3, opts = { thickness: 2 }) =>
  image.drawRectangle(
    rect,
    color,
    opts.thickness,
    cv.LINE_8
  );

export const drawBlueRect = (image: Mat, rect: Rect, opts = { thickness: 2 }) =>
  drawRect(image, rect, new Vec3(255, 0, 0), opts);

export const drawGreenRect = (image: Mat, rect: Rect, opts = { thickness: 2 }) =>
  drawRect(image, rect, new Vec3(0, 255, 0), opts);

export const drawRedRect = (image: Mat, rect: Rect, opts = { thickness: 2 }) =>
  drawRect(image, rect, new Vec3(0, 0, 255), opts);

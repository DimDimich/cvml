import * as cv from 'opencv4nodejs';
import { VideoProcessingCtx } from '../types';

export function rewind(ctx: VideoProcessingCtx, offset: number) {
  let v = ctx.video.get(cv.CAP_PROP_POS_MSEC) + offset;
  if (v < 0) {
    v = 0
  }
  ctx.video.set(cv.CAP_PROP_POS_MSEC, v);
}

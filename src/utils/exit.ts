import * as cv from 'opencv4nodejs';
import { VideoProcessingCtx } from '../types';

export function exit(ctx: VideoProcessingCtx) {
  ctx.video.release();
  cv.destroyAllWindows();
  ctx.timerID && clearInterval(ctx.timerID);
  process.exit(0);
}

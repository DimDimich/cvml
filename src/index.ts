import * as cv from 'opencv4nodejs';

import { resolve } from 'path';
import { CcDetectFaces } from './middleware/CcDetectFaces';
import { KeyboardHandler } from './middleware/KeyboardHandler';
import { ShowFrame } from './middleware/ShowFrame';
import { VideoProcessor } from './VideoProcessor';
import { TrackObjects } from './middleware/TrackObjects';
import { DnnDetectFaces } from './middleware/DnnDetectFaces';

process.on('uncaughtException', err => {
  console.log(err);
  process.exit(1);
});

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

const vp = new VideoProcessor();

const modelsPath = 'models';
const pbFile = resolve(modelsPath, 'ssd_mobilenet_v2_coco_2018_03_29.pb');
const pbTxtFile = resolve(modelsPath, 'ssd_mobilenet_v2_coco_2018_03_29.pbtxt');

// const net = cv.readNetFromTensorflow(pbFile, pbTxtFile);
// Lack of type annotations inside opencv4nodejs
const net = cv.readNetFromTensorflow.apply<null, any[], cv.Net>(null, [pbFile, pbTxtFile]);

vp.use(new DnnDetectFaces(net));
// vp.use(new CcDetectFaces());
// vp.use(new TrackObjects());
vp.use(new KeyboardHandler());
vp.use(new ShowFrame());

// vp.run(resolve(__dirname, '../input/Green-Reception.mp4'));
vp.run(0);
